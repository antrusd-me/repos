## setup apt key

```bash
curl -sL https://gitlab.com/antrusd-me/repos/-/raw/master/apt.key | sudo apt-key add -
```

## setup apt sources

```
# buster
deb https://gitlab.com/antrusd-me/repos/raw/master/debian buster main

# bullseye
deb https://gitlab.com/antrusd-me/repos/raw/master/debian bullseye main

# bookworm
deb https://gitlab.com/antrusd-me/repos/raw/master/debian bookworm main

```
