Format: 3.0 (native)
Source: grub-efi-amd64-signed
Binary: grub-efi-amd64-signed
Architecture: amd64
Version: 1+2.06+2
Maintainer: GRUB Maintainers <pkg-grub-devel@alioth-lists.debian.net>
Uploaders: Felix Zielcke <fzielcke@z-51.de>, Jordi Mallach <jordi@debian.org>, Colin Watson <cjwatson@debian.org>, Ian Campbell <ijc@debian.org>
Standards-Version: 3.9.8
Build-Depends: debhelper (>= 10.1~), sbsigntool [amd64 arm64 i386], grub-efi-amd64-bin (= 2.06-2)
Package-List:
 grub-efi-amd64-signed deb admin optional arch=amd64
Checksums-Sha1:
 4d71f1eb8de693993ab8926e53cb5a25d877169c 4664 grub-efi-amd64-signed_1+2.06+2.tar.xz
Checksums-Sha256:
 5a7a6bd7f3ff4a70689e5aa112c2123d07b739a4c2211c840facbd9dd0bb6d23 4664 grub-efi-amd64-signed_1+2.06+2.tar.xz
Files:
 8ee9bc73e2990526865d893f059ca14b 4664 grub-efi-amd64-signed_1+2.06+2.tar.xz
