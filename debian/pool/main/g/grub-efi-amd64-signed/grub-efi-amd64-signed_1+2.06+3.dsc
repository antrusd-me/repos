Format: 3.0 (native)
Source: grub-efi-amd64-signed
Binary: grub-efi-amd64-signed
Architecture: amd64
Version: 1+2.06+3
Maintainer: GRUB Maintainers <pkg-grub-devel@alioth-lists.debian.net>
Uploaders: Felix Zielcke <fzielcke@z-51.de>, Jordi Mallach <jordi@debian.org>, Colin Watson <cjwatson@debian.org>, Ian Campbell <ijc@debian.org>
Standards-Version: 3.9.8
Build-Depends: debhelper (>= 10.1~), sbsigntool [amd64 arm64 i386], grub-efi-amd64-bin (= 2.06-3~antrusd12u1)
Package-List:
 grub-efi-amd64-signed deb admin optional arch=amd64
Checksums-Sha1:
 437ce8af817042c93db17360671a803f9b8aa2b6 4756 grub-efi-amd64-signed_1+2.06+3.tar.xz
Checksums-Sha256:
 23c4517955274979fdb8e563a8583f8b98663dd810948c2c5c43916174ffb27d 4756 grub-efi-amd64-signed_1+2.06+3.tar.xz
Files:
 e468dbf9d6741268453ed4b18313355c 4756 grub-efi-amd64-signed_1+2.06+3.tar.xz
