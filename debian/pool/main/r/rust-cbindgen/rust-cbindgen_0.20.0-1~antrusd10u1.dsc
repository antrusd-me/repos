Format: 3.0 (quilt)
Source: rust-cbindgen
Binary: librust-cbindgen-dev, librust-cbindgen+clap-dev, cbindgen
Architecture: any
Version: 0.20.0-1~antrusd10u1
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:  Sylvestre Ledru <sylvestre@debian.org>
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/cbindgen
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/cbindgen]
Testsuite: autopkgtest
Testsuite-Triggers: cython3, dh-cargo, g++, gcc, librust-serial-test-0.5+default-dev
Build-Depends: debhelper (>= 12), dh-cargo (>= 24), cargo:native, rustc:native, libstd-rust-dev, librust-clap-2+default-dev, librust-heck-0.3+default-dev, librust-indexmap-1+default-dev, librust-log-0.4+default-dev, librust-proc-macro2-1+default-dev, librust-quote-1+default-dev, librust-serde-1+derive-dev (>= 1.0.103-~~), librust-serde-json-1+default-dev, librust-syn-1+clone-impls-dev (>= 1.0.3-~~), librust-syn-1+extra-traits-dev (>= 1.0.3-~~), librust-syn-1+full-dev (>= 1.0.3-~~), librust-syn-1+parsing-dev (>= 1.0.3-~~), librust-syn-1+printing-dev (>= 1.0.3-~~), librust-tempfile-3+default-dev, librust-toml-0.5+default-dev, help2man, librust-serial-test-dev, cython3
Package-List:
 cbindgen deb utils optional arch=any
 librust-cbindgen+clap-dev deb utils optional arch=any
 librust-cbindgen-dev deb utils optional arch=any
Checksums-Sha1:
 6aec6eca10ca67eb9d4b892af753149899add5c5 126885 rust-cbindgen_0.20.0.orig.tar.gz
 040886e1cce785b86fc50d6e967ccfcebabdcabc 5080 rust-cbindgen_0.20.0-1~antrusd10u1.debian.tar.xz
Checksums-Sha256:
 b39d800c818c49686ac600899ebbb70b0fca4f120ef1a713689f9188c715745f 126885 rust-cbindgen_0.20.0.orig.tar.gz
 268b151aba55c3e4d3d641f3dea2077521065df1f52f8293f01d62852fb1c294 5080 rust-cbindgen_0.20.0-1~antrusd10u1.debian.tar.xz
Files:
 6724e6f80ba70cc7e337562abda77b1e 126885 rust-cbindgen_0.20.0.orig.tar.gz
 537a709973d09e4b7c5010c5ba937e79 5080 rust-cbindgen_0.20.0-1~antrusd10u1.debian.tar.xz
